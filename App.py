from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThreadPool
from GUI.Projekt import Ui_MainWindow
import pyqtgraph as pg
import sys
import os
import random
from Cable import Cable
from PySpice.Spice.NgSpice.Shared import NgSpiceShared
from Signal import Signal
import func

QtCore.QCoreApplication.addLibraryPath("./Libraries/")


class MainApp(QtWidgets.QMainWindow):

    def __init__(self):
        super(MainApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.volt = 5
        self.time = []
        self.bits = []
        self.output = []
        self.signal = Signal(send_data=False)

        self.f_r = 0
        self.f_l = 0
        self.f_c = 0
        self.f_len = 0
        self.b_per_s = 0

        self.hs_v_line = None
        self.he_v_line = None
        self.ls_v_line = None
        self.le_v_line = None

        self.threadpool = QThreadPool()

        self.data_line = None
        self.data_output = None

        self.init_gui_values()

        self.ui.btn_count.clicked.connect(self.start)
        self.ui.btn_rand_bits.clicked.connect(self.randomize_bits)

    def init_gui_values(self):
        self.ui.edit_lenght.setText("10")
        self.ui.edit_R.setText("0.2")
        self.ui.edit_L.setText("9.13")
        self.ui.edit_C.setText("3.65")
        self.ui.edit_bs.setText("80")
        self.ui.combo_signal_type.insertItem(0, "Bits")
        self.ui.combo_signal_type.insertItem(1, "Sinus")
        self.ui.edit_v_range_low_b.setText("0.0")
        self.ui.edit_v_range_low_t.setText("0.8")
        self.ui.edit_v_range_high_b.setText("2.0")
        self.ui.edit_v_range_high_t.setText("5.0")

        input_graph = pg.PlotWidget()
        input_graph.setBackground('w')
        self.ui.plotLayout.addWidget(input_graph)

        output_graph = pg.PlotWidget()
        output_graph.setBackground('w')
        self.ui.plotLayout_2.addWidget(output_graph)

        # input_graph.setXRange(0, 4500, padding=0)
        # input_graph.setYRange(0, 5)
        input_graph.setLabel('left', "<span style=\"color:grey;font-size:15px\">Voltage (V)</span>")
        input_graph.setLabel('bottom', "<span style=\"color:grey;font-size:15px\">Time (1e-11s)</span>")
        input_graph.showGrid(x=True, y=True)
        output_graph.setLabel('left', "<span style=\"color:grey;font-size:15px\">Voltage (V)</span>")
        output_graph.setLabel('bottom', "<span style=\"color:grey;font-size:15px\">Time (1e-11s)</span>")
        output_graph.showGrid(x=True, y=True)
        pen = pg.mkPen(color=(255, 0, 0), width=2)
        pen2 = pg.mkPen(color=(0, 0, 255), width=1, style=QtCore.Qt.DashLine)
        pen3 = pg.mkPen(color=(0, 255, 0), width=1, style=QtCore.Qt.DashLine)

        self.data_line = input_graph.plot(self.time, self.bits, pen=pen)
        self.data_output = output_graph.plot(self.time, self.output, pen=pen)
        self.hs_v_line = output_graph.addLine(y=float(self.ui.edit_v_range_high_t.text()), pen=pen2)
        self.he_v_line = output_graph.addLine(y=float(self.ui.edit_v_range_high_b.text()), pen=pen2)
        self.ls_v_line = output_graph.addLine(y=float(self.ui.edit_v_range_low_t.text()), pen=pen3)
        self.le_v_line = output_graph.addLine(y=float(self.ui.edit_v_range_low_b.text()), pen=pen3)

    def randomize_bits(self):
        self.bits = [random.randint(0, 1) for i in range(8)]
        bits_str = "".join(str(s) for s in self.bits)
        self.ui.edit_bits.setText(bits_str)

    def update_plot_data(self, data):
        self.data_line.setData(data.input)
        self.data_output.setData(data.output)

    def calc_finish(self):
        self.ui.label_state.setStyleSheet("QLabel { color : green; }")
        self.ui.label_state.setText("Gotowe")
        self.ui.btn_count.setEnabled(True)

    def get_data_from_form(self):
        s_bits = self.ui.edit_bits.text()
        self.f_r = float(self.ui.edit_R.text())
        self.f_l = float(self.ui.edit_L.text())
        self.f_c = float(self.ui.edit_C.text())
        self.f_len = float(self.ui.edit_lenght.text())
        self.b_per_s = int(self.ui.edit_bs.text())
        self.bits = []
        if s_bits == "":
            self.randomize_bits()
        for b in s_bits:
            if b == '1' or b == '0':
                self.bits.append(int(b))
            else:
                self.ui.label_state.setStyleSheet("QLabel { color : red; }")
                self.ui.label_state.setText("W polu dla bitów proszę wpisać jedynie cyfry 0 lub 1")
                return False
        if len(self.bits) != 8:
            self.ui.label_state.setStyleSheet("QLabel { color : red; }")
            self.ui.label_state.setText("Proszę wpisać 8 bitów")
            return False
        return True

    def start(self):
        self.ui.btn_count.setEnabled(False)
        status = self.get_data_from_form()
        if not status:
            self.ui.btn_count.setEnabled(True)
            return
        self.ui.label_state.setStyleSheet("QLabel { color : red; }")
        self.ui.label_state.setText("Trwa przetwarzanie...")
        volt_high = float(self.ui.edit_v_range_high_t.text())
        volt_high_end = float(self.ui.edit_v_range_high_b.text())
        volt_low = float(self.ui.edit_v_range_low_b.text())
        volt_low_beg = float(self.ui.edit_v_range_low_t.text())
        func_type = self.ui.combo_signal_type.currentIndex()

        self.hs_v_line.setValue(volt_high)
        self.he_v_line.setValue(volt_high_end)
        self.ls_v_line.setValue(volt_low_beg)
        self.le_v_line.setValue(volt_low)

        if func_type == 0:
            function = func.func_bits
        else:
            function = func.func_sin

        period = 1/(self.b_per_s*1e6*8)
        print(period)
        self.signal.set_data(amplitude=volt_high, func=function, v_low=volt_low, bits=self.bits, period=period)
        cable = Cable()
        cable.set_data(self.signal, self.f_r, self.f_l, self.f_c, self.f_len, period*10)

        cable.signals.result.connect(self.update_plot_data)
        cable.signals.finished.connect(self.calc_finish)

        self.threadpool.start(cable)


path_lib = os.getcwd()
path_lib = path_lib + r"\Libraries\Spice64_dll"
path_dll = path_lib
path_dll = path_dll + r"\dll-vs\ngspice.dll"
NgSpiceShared.LIBRARY_PATH = path_dll
NgSpiceShared.NGSPICE_PATH = path_lib
app = QtWidgets.QApplication([])
application = MainApp()
application.show()
sys.exit(app.exec())
