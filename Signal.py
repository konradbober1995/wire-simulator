import PySpice.Logging.Logging as Logging
from PySpice.Spice.NgSpice.Shared import NgSpiceShared

logger = Logging.setup_logging()


class Signal(NgSpiceShared):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._amplitude = 0
        self.func = None
        self.bits = None
        self.volt_low = 0
        self.period = 0

    def set_data(self, amplitude, func, v_low, bits, period=1e-7 / 8):
        self._amplitude = amplitude
        self.func = func
        self.bits = bits
        self.volt_low = v_low
        self.period = period

    def get_vsrc_data(self, voltage, time, node, ngspice_id):
        self._logger.debug('ngspice_id-{} get_vsrc_data @{} node {}'.format(ngspice_id, time, node))
        voltage[0] = (self._amplitude-self.volt_low) * self.func(time, self.bits, self.period) + self.volt_low
        return 0

    def get_isrc_data(self, current, time, node, ngspice_id):
        self._logger.debug('ngspice_id-{} get_isrc_data @{} node {}'.format(ngspice_id, time, node))
        current[0] = 1.
        return 0


