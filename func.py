import math


def func_bits(time, bits, duration):
    num = int(time / duration)
    if num > 7:
        return 0
    else:
        return bits[num]


def func_sin(time, bits, duration):
    return math.sin(time*2e8)/2 + 0.5