from PySpice.Spice.Netlist import Circuit
from PySpice.Unit import *
from PyQt5.QtCore import QRunnable, pyqtSignal, QObject


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Cable(QRunnable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.signal = None
        self.r = 0
        self.l = 0
        self.c = 0
        self.length = 0
        self.time = 0
        self.signals = WorkerSignals()

    def run(self):
        circuit = Circuit('Transmission Line')
        #circuit.PulseVoltageSource('pulse', 'input', circuit.gnd, 0 @ u_V, 1 @ u_V, 1 @ u_ns, 1 @ u_us)

        circuit.LossyTransmission('1', 'output', circuit.gnd, 'input', circuit.gnd, model="LOSS")
        circuit.model("LOSS", "LTRA", len=self.length, R=self.r, G=0, L=self.l*1e-9, C=self.c*1e-12)
        circuit.V('input', 'input', circuit.gnd, 'dc 0 external')
        circuit.R('load', 'output', circuit.gnd, 50@u_Ω)
        simulator = circuit.simulator(temperature=25, nominal_temperature=25,
                                     simulator='ngspice-shared', ngspice_shared=self.signal)
        analysis = simulator.transient(step_time=self.time/(10 * 100 * 5), end_time=self.time)

        self.signals.result.emit(analysis)
        self.signals.finished.emit()

    def set_data(self, signal, r, l, c, length, time):
        self.signal = signal
        self.r = r
        self.l = l
        self.c = c
        self.length = length
        self.time = time
